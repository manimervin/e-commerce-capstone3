import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){

	// console.log(props)

	const { itemsProp, fetchData } = props;

	const [itemsArr, setItemsArr] = useState([])
	const [itemId, setItemId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [quantity, setQuantity] = useState(0)
	const [price, setPrice] = useState(0)
	const [showAdd, setShowAdd] = useState(false)
	const [showEdit, setShowEdit] = useState(false)

	const token = localStorage.getItem("token")

	//Functions to handle opening and closing modals
	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)

	const openEdit = (itemId) => {
		//fetch request with the course's ID
		fetch(`https://rocky-reaches-97141.herokuapp.com/api/items/${itemId}`)
		.then(res => res.json())
		.then(data => {
			setItemId(data._id)
			setName(data.name)
			setDescription(data.description)
			setQuantity(data.quantity)
			setPrice(data.price)
			// console.log(data)
		})
		//populate the edit modal's input fields with the proper information
		setShowEdit(true)

	}

	const closeEdit = () => {
		setName("")
		setDescription("")
		setPrice(0)
		setQuantity("")
		setShowEdit(false)
	}

	const addItem = (e) => {
		e.preventDefault()

		fetch(`https://rocky-reaches-97141.herokuapp.com/api/items/additem`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Item successfully added"
				})

				fetchData()
				closeAdd()

				setName("")
				setDescription("")
				setQuantity("")
				setPrice(0)
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})

				fetchData()		
			}
		})
	}

	const editItem = (e) => {
		e.preventDefault()

		fetch(`https://rocky-reaches-97141.herokuapp.com/api/items/updateItem/${itemId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Item successfully updated"
				})

				fetchData()
				closeEdit()

				setName("")
				setDescription("")
				setQuantity("")
				setPrice(0)
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})

				fetchData()		
			}
		})
	}

	const archiveToggle = (itemId, isActive) => {
		fetch(`https://rocky-reaches-97141.herokuapp.com/api/items/archive/${itemId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data){
				let bool

				isActive ? bool = "disabled" : bool = "enabled"

				fetchData()

				Swal.fire({
					title: "Sucess",
					icon: "success",
					text: `Item successfully ${bool}`
				})
			}else{
				fetchData()

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}

		})
	}

	useEffect(() => {
		const items = itemsProp.map(item => {
			return(
				<tr key={item._id}>
					<td>{item.name}</td>
					<td>{item.description}</td>
					<td>{item.quantity}</td>
					<td>{item.price}</td>
					<td>
							{item.isActive
								? <span>Available</span>
								: <span>Unavailable</span>
							}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(item._id)}>Update</Button>
						{item.isActive
							? <Button variant="danger" size="sm" onClick={() => archiveToggle(item._id, item.isActive)}>Disable</Button>
							: <Button variant="success" size="sm" onClick={() => archiveToggle(item._id, item.isActive)}>Enable</Button>
						}
					</td>
				</tr>
			)
		})

		setItemsArr(items)

	}, [itemsProp])
	
	return(
		<>
			<div className="text-center my-4">
				<h2>Item Management</h2>
				<Button variant="primary" onClick={openAdd}>Add New Item</Button>
			</div>

			{/*Course info table*/}
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{itemsArr}
				</tbody>
			</Table>

			
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addItem(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="itemName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="itemDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="itemQuantity">
							<Form.Label>Quantity</Form.Label>
							<Form.Control
								value={quantity}
								onChange={e => setQuantity(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>

						<Form.Group controlId="itemPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>x</Button>
						<Button variant="success" type="submit">Add</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editItem(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Item</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="itemName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="itemDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="itemQuantity">
							<Form.Label>Quantity</Form.Label>
							<Form.Control
								value={quantity}
								onChange={e => setQuantity(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>

						<Form.Group controlId="itemPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>x</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
	)
}
