import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner() {

	return(

		<Row>
			<Col className="p-5">
				<h1>Rocky Reaches</h1>
				<p>Your source of soulful books</p>
				<Button variant="primary" as={Link} to={`/items/`}>Shop Now</Button>
			</Col>
		</Row>

		)

}