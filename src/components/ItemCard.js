//import { useState, useEffect } from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ItemCard({itemProp}) {

	
	const {name, price, _id} = itemProp

	return(
		<Row className="d-inline-flex m-2 shadow md={4}">
			<Card>
			  <Card.Img variant="top" src="https://image.shutterstock.com/image-vector/new-born-essentials-collection-baby-260nw-1796650339.jpg" />
			  <Card.Body>
			    <Card.Title>{name}</Card.Title>
			    <Card.Subtitle>Php {price}</Card.Subtitle>
			    <Card.Text></Card.Text>

			    <Button variant="dark" as={Link} to={`/items/${_id}`}>Details</Button>
			  </Card.Body>
			</Card>
		</Row>					
		)

}





