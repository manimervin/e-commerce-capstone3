import { useState, useEffect } from 'react';
// import courseData from './../data/courses'
import ItemCard from './ItemCard'

export default function UserView({itemsProp}) {

	const [itemsArr, setItemArr] = useState([])

	// console.log(courseData)

	//on component mount/page load, useEffect will run and call our fetchData function, which runs our fetch request
	useEffect(() => {
		const items = itemsProp.map(item => {
			// console.log(course)
			if(item.isActive){
				return <ItemCard key={item._id} itemProp={item} />
			}else{
				return null
			}
		})

		setItemArr(items)

	}, [itemsProp])

	return(
		<>
			{itemsArr}
		</>
	)
}
