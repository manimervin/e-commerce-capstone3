import {Fragment} from 'react';
//import {Container } from 'react-bootstrap'
import Banner from '../components/Banner';
//import CourseCard from '../components/ItemCard';
import Highlights from '../components/Highlights';
// import Cart from '../components/Cart';

export default function Home() {


	return(

		<Fragment>
			<Banner/>
			<Highlights/>
			{/*<ItemCard/>*/}
		</Fragment>
		)
}