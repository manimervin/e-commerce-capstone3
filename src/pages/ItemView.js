import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ItemView() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const { itemId } = useParams();
	
	const [name, setName] = useState("");
	const [quantity, setQuantity] = useState(0);
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const addToCart = (itemId) => {

		fetch("https://rocky-reaches-97141.herokuapp.com/api/users/createorder", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify([{
				itemId: itemId,
				quantity: quantity
			}])
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)

			if (data === true){

				Swal.fire({
					title: 'Sucessfully Order',
					icon: "success",
					text: "You have successfully ordered this item."
				})

				navigate("/items")

			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: "error",
					text: "Please try again"	
				})
			}
		})
	}

	useEffect(() => {
		//console.log(itemId)
		fetch(`https://rocky-reaches-97141.herokuapp.com/api/items/${itemId}`)
		.then(res => res.json())
		.then(data => {
			//console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setQuantity(data.quantity)
		})

	}, [itemId])

	return(
			<Container className="mt-5">
				<Row>
					<Col lg={{span:6, offset:3}}>
						<Card>
							<Card.Body className = "text-center shadow">
								<Card.Img variant="right" src="https://w7.pngwing.com/pngs/385/252/png-transparent-baby-transport-infant-blanket-bugaboo-international-baby-toddler-car-seats-essentials-miscellaneous-child-textile-thumbnail.png" />
								<Card.Title><h1>{name}</h1></Card.Title>
								<Card.Text>{description}</Card.Text>
								<Card.Text><h3>Php <strong>{price}</strong></h3></Card.Text>
								<Form.Group controlId="itemQuantity">
									<Form.Label>Quantity</Form.Label>
									<Form.Control
										value={quantity}	
										onChange={e => setQuantity(e.target.value)}
										type="number"
										required
									/>
								</Form.Group>
								{
									user.id !== null ?
									<Button variant = "primary" onClick={() =>
											addToCart(itemId)}>Create Order</Button>	
										:

									<Link className="btn btn-danger" to="/login">Log in to Order</Link>
								}			
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		)
}