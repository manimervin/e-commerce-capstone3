import { useState, useEffect, useContext } from 'react';
import UserView from  "../components/UserView"
import AdminView from  "../components/AdminView"
import UserContext from "../UserContext"

export default function Items() {

	const { user } = useContext(UserContext);
	//console.log(user)
	const [itemsData, setItemsData] = useState([])

	const fetchData = () => {
		fetch(`https://rocky-reaches-97141.herokuapp.com/api/items/allItems`)
		.then(res => res.json())
		.then(data => {
			//.then has what is called a "self-contained scope"

			//Any code inside of this .then only exists inside of this .then, and therefore cannot be processed properly by React

			//to solve this problem, we use a state. By setting the new value of our state to be the data from our server, that state can be seen by our entire component
			setItemsData(data)
			// console.log(data)
		
		})
	}

	useEffect(() => {
			fetchData()
		}, [])

		return(user.isAdmin ?
			<AdminView itemsProp={itemsData} fetchData={fetchData}/>
			:
			<UserView itemsProp={itemsData}/>)
	}
