import { useState, useEffect, useContext } from "react";
import { Container, Button, Card, Row } from "react-bootstrap";
import UserContext from "../UserContext";
import { Link } from "react-router-dom";

export default function OrderItems() {
  const [ orders, setOrders ] = useState([]);
  const [ orderItems, setOrderItems ] = useState();
  const token = localStorage.getItem("token");
  const {user, setUser} = useContext(UserContext);

  useEffect(() => {
      fetch("https://rocky-reaches-97141.herokuapp.com/api/users/userOrders", {
        headers: {
          Authorization: `Bearer ${ token }`
      }
      })
      .then(res => res.json())
      .then(data => {
      	  setOrders(data)
    	  displayOrder()
      })
  },)
  	//console.log(orders)
  	
  	function displayOrder(){
  		setOrderItems (orders.orderItems.map(
  	        (itemdata) => {
  		            return (
  		                <Row className="d-inline-flex m-2 shadow md={4}">
  		                	<Card>
  		                		<Card.Title>
  		                			{itemdata.itemId}
  		                		</Card.Title>
  		                		<Card.Subtitle>
  		                			{itemdata.orderedOn}
  		                		</Card.Subtitle>
  		                		<Card.Text>
  		                			{itemdata.status}
  		                		</Card.Text>
  		                	</Card>
  		                </Row>
  		            )
  		        }
  		    ))	
  	}

	
return (
  	
    (user.id !== null) ?
    	<Container>
    		<h1 className="text-center my-3">Order History</h1>
    				{orderItems ? orderItems : 'No Order History'}
    	</Container>
    	: 
    	<Container className="my-5 text-center">
    		<h1>Please Log in to view order</h1>
    	<Button variant="danger" as={Link} to={"/login"}>Sign in</Button>
    
  		</Container>
)
}
